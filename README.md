# Getting Started

## Prerequisities

In order to run this container you'll need docker installed.


## Steps to build a Docker image:

1. Pull image from [DockerHub](https://hub.docker.com/r/kalizhaankyzy/cvd-stat) 

        docker pull kalizhaankyzy/cvd-stat

2. Run installed image in container named `cvd-stat`. The `-p` option forwards the container's port 3000 to port 5000 on the host. The `-d` option runs container in background.

        docker run -p 5000:3000 -d --name cvd-stat kalizhaankyzy/cvd-stat

## Steps to build React App:

1. Install `node` and `npm` libraries

        npm install

2. Run the app in the development mode. 
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
        
        npm start

The page will reload when you make changes.\
You may also see any lint errors in the console.


## Built With
* React
* Javascript
* CSS

## The data was taken from [corona.lmao.ninja](https://corona.lmao.ninja)
